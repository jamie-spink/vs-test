import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import parseLinkHeader from 'parse-link-header'

Vue.config.productionTip = false
Vue.prototype.$http = axios
Vue.prototype.$parseLinkHeader = parseLinkHeader

new Vue({
  render: h => h(App),
}).$mount('#app')
